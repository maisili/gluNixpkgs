{
  description = "gluNixpkgs";

  outputs = { self, mycin, golMycin }:
  {
    strok = {
      praim = { djenyreicyn = 6; spici = "indeks"; };
      spici = {
        allSystems = "niks";
      };
    };

    datom = {
      archToNixpkgsSystem = arch: {
        "x86-64" = "x86_64-linux";
        "i686" = "i686-linux";
        "aarch64" = "aarch64-linux";
        "armv7l" = "armv7l-linux";
        "avr" = "avr-none";
      }.${arch};

      allSystems = let inherit (self.datom) archToNixpkgsSystem;
      in {
        localSystem = {
          system = archToNixpkgsSystem mycin.datom.tcipSet.architecture;
        };
        crossSystem = {
          system = archToNixpkgsSystem golMycin.datom.tcipSet.architecture;
        };
      };

    };

  };
}
